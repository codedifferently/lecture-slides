# JavaScript and SQL

-
-

## Lecture Overview

- JavaScript MySQL Connectors
- Sequelize

-
-

## Connector

-

### Setup

```sql
-- Run once to setup MySQL
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'password';
flush privileges;
```

```javascript
let mysql = require('mysql');

let con = mysql.createConnection({
  host: "localhost",
  user: "yourusername",
  password: "yourpassword",
  database: "mydb"
});
```

-

### Queries

```javascript
con.connect(err => {
  if(err) {
    throw err;
  }
  con.query("SELECT * FROM customers", (err, result, fields) => {
    if (err){
        throw err;
    }
    console.log(result);
  });
});
```

-
-

## Sequilize

-

### Setup

```bash
npm install mysql2 sequelize
```

```javascript
// Initialize Connection
const Sequelize = require('sequelize');
let sequelize = new Sequelize('mysql://root:password@:port/dbname');

//Define table models
let Users = sequelize.define('users', {
    name: Sequelize.STRING,
    age: Sequelize.INTEGER,
    username: Sequelize.STRING
});

```

-

### Queries

```javascript
//Select *
Users.findAll().then( users => {
    console.log(users);
})

//Insert into
Users.create({
    name: "Kaleb Burd",
    age: 24,
    username: "kburd"
})
```

*Note: both methods return Promises*
