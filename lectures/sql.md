# SQL

-
-

## Lecture Overview

- Querying data using SQL
- Modifying data using SQL
- Creating Schema's
- Relational Databases

-
-

## Querying Data

-

### Simple Queries

Documentation

```sql
--Retrieves all the data from the table
SELECT * FROM <table>;

--Retrieves all the data from a specific column in the table
SELECT <column> FROM <table>;

--Retrieves all the data from the specific columns in the table
SELECT <column1>, <column2> FROM <table>;
```

Example

```sql
SELECT full_name, age, username FROM users;
```

```text
|name       |age|username |
---------------------------
|Kaleb Burd |24 |kburd    |
|Emma Watson|30 |hGranger |
|Tariq Hook |40 |CodeRhino|
```

-

### Filtering w/ Comparisons

Documentation

```sql
--Retrieves all the data from the table where condition is true
SELECT * FROM <table> WHERE <condition>;

--Retrieves all the data from the table where both conditions are true
SELECT * FROM <table> WHERE <condition1> AND <condition2>;

--Retrieves all the data from the table where either condition is true
SELECT * FROM <table> WHERE <condition1> OR <condition2>;
```

Example

```sql
SELECT * FROM users where age < 40;
```

```text
|full_name  |age|username |
---------------------------
|Kaleb Burd |24 |kburd    |
|Emma Watson|30 |hGranger |
```

-

### Filtering w/ Ranges

Documentation

```sql
--Retrieves all the data from the table where condition is true
SELECT * FROM <table> WHERE <column> IN (value1, value2);

--Retrieves all the data from the table where both conditions are true
SELECT * FROM <table> WHERE <column> BETWEEN <value1> and <value2>;
```

Example

```sql
SELECT * FROM users where age BETWEEN 25 AND 45;
```

```text
|full_name  |age|username |
---------------------------
|Emma Watson|30 |hGranger |
|Tariq Hook |40 |CodeRhino|
```

-

### Filtering w/ Pattern Matching

Documentation

```sql
--Retrieves all the data from the table where column data end with pattern
SELECT * FROM <table> WHERE <column> LIKE "%<text>";

--Retrieves all the data from the table where column data starts with pattern
SELECT * FROM <table> WHERE <column> LIKE "<text>%";

--Retrieves all the data from the table where column data contains text
SELECT * FROM <table> WHERE <column> LIKE "%<text>%";
```

Example

```sql
SELECT * FROM users where username LIKE "r%";
```

```text
|full_name  |age|username |
---------------------------
|Tariq Hook |40 |CodeRhino|
```

-

### Misc

```sql
--Gives column an alias in report
SELECT <column> AS <alias> FROM <table>;

--IS can be used interchangably with "="
SELECT * FROM <table> WHERE <column> IS <value>;

--NOT inverts a condition
SELECT * FROM <table> WHERE <column> IS NOT <value>;

--Orders the results ascending
SELECT <column> FROM <table> ORDER BY <column>;

--Orders the results descending
SELECT <column> FROM <table> ORDER BY <column> DESC;
```

-
-

## Modifying Data

-

### Creating Records

Documentation

```sql
-- Inserting a single row
INSERT INTO <table> VALUES (<value 1>, <value 2>);

-- Inserting a single row with values in any order
INSERT INTO <table> (<column 1>, <column 2>) 
    VALUES (<value 1>, <value 2>);
```

Example

```sql
INSERT INTO users (full_name, age, username) 
    VALUES ("Peter Parker", "16", "spiderman");
```

```text
|full_name        |age  |username     |
---------------------------------------
|Kaleb Burd   |24   |kburd     |
|Emma Watson  |30   |hGranger  |
|Tariq Hook   |40   |CodeRhino |
|Peter Parker |16   |spiderman |
```

-

### Creating Multiple Records

Documentation

```sql
-- Inserting multiple rows in a single statement
INSERT INTO <table> (<column 1>, <column 2>) 
    VALUES 
        (<value 1>, <value 2>),
        (<value 1>, <value 2>),
        (<value 1>, <value 2>);
```

Example

```sql
INSERT INTO users (full_name, age, username) 
    VALUES 
        ("Robert Downey Jr.", "55", "i_am_ironman"),
        ("Thanos", "1000", "madTitan");
```

```text
|full_name        |age  |username     |
---------------------------------------
|Kaleb Burd       |24   |kburd        |
|Emma Watson      |30   |hGranger     |
|Tariq Hook       |40   |CodeRhino    |
|Robert Downey Jr.|55   |i_am_ironman |
|Thanos           |1000 |madTitan     |
```

-

### Updating Records

Documentation

```sql
-- An update statement for all rows
UPDATE <table> SET <column> = <value>;

-- Update multiple columns in all rows
UDPATE <table> SET <column 1> = <value 1>, <column 2> = <value 2>;

-- An update statement for specific rows
UPDATE <table> SET <column> = <value> WHERE <condition>;

```

Example

```sql
UPDATE users SET age = 30 WHERE age < 30;
```

```text
|full_name        |age  |username     |
---------------------------------------
|Kaleb Burd       |30   |kburd        |
|Emma Watson      |30   |hGranger     |
|Tariq Hook       |40   |CodeRhino    |
```

-

### Deleteing Records

Documentation

```sql
-- To delete all rows from a table
DELETE FROM <table>;

-- To delete specific rows from a table
DELETE FROM <table> WHERE <condition>;
```

Example

```sql
DELETE FROM users WHERE age > 25;
```

```text
|full_name        |age  |username     |
---------------------------------------
|Kaleb Burd       |30   |kburd        |
```

-

### Transactions

Documentation

```sql
-- Switch autocommit off and begin a transaction
BEGIN;

-- Save results of the statements to the database
COMMIT;

-- Resets the state of the database to before the transaction
ROLLBACK;
```

Example

```sql
BEGIN;
DELETE FROM users WHERE age > 25;
ROLLBACK;
```

```text
|full_name        |age  |username     |
---------------------------------------
|Kaleb Burd       |30   |kburd        |
|Emma Watson      |30   |hGranger     |
|Tariq Hook       |40   |CodeRhino    |
```

-
-

## Tables

-

### Creating Tables

Documentation

```sql
CREATE TABLE <tableName> (
    <column1> <dataType>,
    <column2> <dataType>,
    <column3> <dataType>,
);
```

Example

```sql
CREATE TABLE users (
    name VARCHAR(256),
    age INT,
    username VARCHAR(256),
);
```

```text
|name|age |username|
--------------------
|null|null|null    |
```

-

### Creating Tables (cont.)

Documentation

```sql
CREATE TABLE <tableName> (
    <column1> <dataType>,
    <column2> <dataType> NOT NULL,
    <column3> <dataType> DEFAULT <value>
);
-- Column 2 will not allow null values
-- Column 3 will use defualt value if one is not provided
```

Example

```sql
CREATE TABLE users (
    name VARCHAR(256) NOT NULL,
    age INT DEFAULT 0,
    username VARCHAR(256)
);
INSERT INTO users (name) VALUES ("Kaleb");
```

```text
|name |age|username|
--------------------
|Kaleb|0  |null    |
```

-

### Adding Keys

Documentation

```sql
CREATE TABLE <tableName> (
    <primary_key_name> <dataType>,
    <foreign_key_name> <dataType>, 
    <column3> <dataType>,
    AUTO_INCREMENT PRIMARY KEY(<primary_key_name>)
    FOREIGN KEY (<foreign_key_name>) REFERENCES <table>(<column>)
);
```

Example

```sql
CREATE TABLE users (
    ID INT AUTO_INCREMENT,
    SSN VARCHAR(9),
    PRIMARY KEY (ID),
    FOREIGN KEY (SSN) REFERENCES tax(SSN)  
);
```

```text
|ID  |SSN |age |
----------------
|null|null|null|
```

-

### Altering Tables

Documentation

```sql
-- Add Column
ALTER TABLE <tableName> ADD COLUMN <columnName> <dataType>;

-- Drop Column
ALTER TABLE <tableName> DROP COLUMN <columnName>;

-- Change column data type
ALTER TABLE <tableName> MODIFY COLUMN <columnName> <dataType>;
```

Example

```sql
CREATE TABLE users ( 
    name VARCHAR(256), age INT, username VARCHAR(256)
); 
ALTER TABLE users DROP COLUMN age;
ALTER TABLE users ADD COLUMN dob Date;
```

```text
|name|dob |username|
--------------------
|null|null|null    |

```

-

### Removing Tables

```sql
-- Remove Table
DROP TABLE <tableName>;

-- Remove Table if it exists
DROP TABLE IF EXISTS <tableName>;
```

-
-

## Relational Databases

-

### Keys

![diagram](https://www.sqlshack.com/wp-content/uploads/2020/02/foreign-key-data-matching-illustration.png)

**Primary Key:** A unique identifier for a record in a table
**Foreign Key:** An identifier that corrdinates to a record in another table

-

### Cardinality

<img src="https://upload.wikimedia.org/wikipedia/commons/0/04/Entity_Relationship_Diagram_Examples.png" height="550px" width="500px">

-

### Junction Tables

![diagram](https://blog.devart.com/wp-content/uploads/2010/06/Junction-Table-example.png)

-

### Joins

Documentation

```sql
-- Inner Join
SELECT <columns> FROM <table1> INNER JOIN <table2> 
    ON <condition> where <condition>;

-- Outer Column
SELECT <columns> FROM <table1> <side> OUTER JOIN <table2> 
    ON <condition> where <condition>;
```

Example

```sql
SELECT * FROM users INNER JOIN roles 
    ON users.userId = role.userId;
```

```text
|userId|name |dob       |username |roleName  |
----------------------------------------------
|10    |Kaleb|06/22/1996|kburd    |instructor|
|11    |Tariq|01/31/1978|CodeRhino|admin     |
|12    |Emma |08/12/1992|CodeRhino|student   |
```

-

### Set Operations

![diagram](https://blogg.itverket.no/content/images/2019/06/set-theory.png)

```sql
<query1> UNION <query2>
<query1> UNION ALL <query2>
<query1> INTERSECT <query2>
<query1> EXCEPT <query2>
```

Examples

```sql
select name from fruit UNION select name from vegetable;
select name from vegetable INTERSECT select name from fruit;
select name from fruit EXCEPT select name from vegetable;
```

-

### Subquery

Documentation

```sql
-- Subquery using IN
SELECT <columns> FROM <table> WHERE <column> IN (<subquery>);

-- Subquery using temp table
SELECT <columns> FROM <table1> INNER JOIN 
    (SELECT <column> FROM <table2> WHERE <condition>) AS <alias>
    ON <table1>.<column1> = <alias>.<column2>;
```

Example

```sql
SELECT name FROM users WHERE name IN (SELECT name FROM bannedUsers);
```

```text
|name  |
--------
|Thanos|
```
