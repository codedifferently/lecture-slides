
from flask import Flask, request
from flask_cors import CORS
import os

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, 
            static_url_path='',
            static_folder='')

CORS(app)

@app.route('/')
def root():
    return app.send_static_file('./index.html')

@app.route('/lectures')
def lectures():
    print(os.listdir("./lectures"))
    return str(os.listdir("./lectures"))

if __name__ == "__main__":
    app.run()