# Code Differently Lecture Slides

## Description

* The purpose of this repository is to create a centralized location for CodeDifferently slides.

## Developmental Notes

* Please ensure that lecture slides are written in a manner that allows any technically competent person to give the presentation.
* Slides should not include personalized notes
* Slides should not enforce a particular presentation-style

## Viewing the application

* The application can be ran by executing the following command from the root directory of the project.
  * `python main.py`
* After running the application, navigate to `localhost:5000/index.html` from a browser to view the slides.
