
// const server = "http://hex36.pythonanywhere.com/"
const server = "http://localhost:5000/"


function pdfExportSetup(){
    var link = document.createElement( 'link' );
    link.rel = 'stylesheet';
    link.type = 'text/css';
    link.href = window.location.search.match( /print-pdf/gi ) ? './resources/css/print/pdf.css' : './resources/css/print/paper.css';
    document.getElementsByTagName( 'head' )[0].appendChild( link );
}

function setLecture(){
    let lecutureName = sessionStorage.getItem("lecture");

    document.getElementById("slides").innerHTML += `
        <section id="markdown"
            data-markdown="${server}/lectures/${lecutureName}.md"
            data-separator="^-\n-\n"
            data-separator-vertical="^-\n"
            data-separator-notes="^Note:"
            data-charset="utf-8">
        </section>`
}

function initializeReveal(){
    Reveal.initialize({
        controls: true,
        progress: true,
        history: true,
        center: true,

        transition: 'slide', // none/fade/slide/convex/concave/zoom

        // Optional reveal.js plugins
        dependencies: [
            { src: './resources/lib/js/classList.js', condition: function() { return !document.body.classList; } },
            { src: './resources/plugin/markdown/marked.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
            { src: './resources/plugin/markdown/markdown.js', condition: function() { return !!document.querySelector( '[data-markdown]' ); } },
            { src: './resources/plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
            { src: './resources/plugin/zoom-js/zoom.js', async: true },
            { src: './resources/plugin/notes/notes.js', async: true }
        ]
    });
}

pdfExportSetup();
setLecture();
initializeReveal();