
// const server = "http://hex36.pythonanywhere.com/"
const server = "http://localhost:5000/"


function loadLecture(name){
    sessionStorage.setItem("lecture", name)
}

function generateLectureList(){

    fetch(`${server}/lectures`)
        .then(response => response.text())
        .then(text => text.replaceAll("'", '\"'))
        .then(text => JSON.parse(text))
        .then(filenames => filenames.map( filename => {
            let len = filename.length;
            return filename.substring(0, len-3)
        }))
        .then(lectures => {
            document.getElementById("lectures").innerHTML = lectures
                .map(lecture => 
                    `<li>
                        <a href="lecture.html" onclick="loadLecture('${lecture}')">
                            ${filenameToLabel(lecture)}
                        </a>
                    </li>`
                ).join("")
        });
        
}

function filenameToLabel(filename){
    return filename.split('-').map(name => name[0].toUpperCase() + name.substring(1, name.length)).join(" ");
}

generateLectureList();